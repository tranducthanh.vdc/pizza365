package com.pizza365.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.pizza365.model.Products;
import com.pizza365.repository.ProductRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class ProductController {
@Autowired
private ProductRepository productRepo;
@GetMapping("/products")
public List<Products> getAllProducts() {
	return productRepo.findAll();
}


//Get  product by productsCode
	@GetMapping("/productsCode={productsCode}")
	public List<Products> getProductsByCode(@PathVariable("productsCode") String productsCode) {
		return productRepo.findProductsByCode("%" + productsCode + "%");
	}

	// Get product by quantity
	@GetMapping("/productQuantity={quantity}")
	public List<Products> getProductsByQuantity(@PathVariable("quantity") String quantity) {
		return productRepo.findProductsByQuantity(quantity);
	}
	//  by Price DESC
	@GetMapping("/buyPrice/DESC")
	public List<Products> getCustomerDESCByid() {
		return productRepo.getProductsPriceDESC();
	}

// Get product by id
@GetMapping("/product/{id}")
public Products getProductById(@PathVariable Long id) {
	if (productRepo.findById(id).isPresent())
		return productRepo.findById(id).get();
	else
		return null;
}

// create new product
@PostMapping("/product/create")
public ResponseEntity<Products> createNewProduct(@RequestBody Products cproduct) {
	try {
		Products newProduct = new Products();
		newProduct.setProductCode(cproduct.getProductCode());
		newProduct.setProductName(cproduct.getProductCode());
		newProduct.setProductDescription(cproduct.getProductDescription());
		newProduct.setProductLines(cproduct.getProductLines());
		newProduct.setProductScale(cproduct.getProductScale());
		newProduct.setProductVendor(cproduct.getProductVendor());
		newProduct.setQuantityInStock(cproduct.getQuantityInStock());
		newProduct.setBuyPrice(cproduct.getBuyPrice());
		Products saveProduct = productRepo.save(newProduct);
		return new ResponseEntity<>(saveProduct, HttpStatus.CREATED);
	} catch (Exception e) {
		System.out.println(e);
		return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	}
}

// edit product by id customer
@PutMapping("/product/update/{id}")
public ResponseEntity<Object> updateProduct(@PathVariable("id") Long id, @RequestBody Products cproduct) {
	Optional<Products> productData = productRepo.findById(id);
	if (productData.isPresent()) {
		Products newProduct = new Products();
		newProduct.setProductCode(cproduct.getProductCode());
		newProduct.setProductName(cproduct.getProductCode());
		newProduct.setProductDescription(cproduct.getProductDescription());
		newProduct.setProductLines(cproduct.getProductLines());
		newProduct.setProductScale(cproduct.getProductScale());
		newProduct.setProductVendor(cproduct.getProductVendor());
		newProduct.setQuantityInStock(cproduct.getQuantityInStock());
		newProduct.setBuyPrice(cproduct.getBuyPrice());
		Products saveProduct = productRepo.save(newProduct);
		return new ResponseEntity<>(saveProduct, HttpStatus.OK);
	} else {
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
}
// delete product by Id
@DeleteMapping("/product/delete/{id}")
public ResponseEntity<Object> deleteCustomerById(@PathVariable Long id) {
	try {
		productRepo.deleteById(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	} catch (Exception e) {
		System.out.println(e);
		return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
}
