package com.pizza365.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pizza365.model.Offices;
import com.pizza365.repository.OfficeRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class OfficeController {
	@Autowired
	private OfficeRepository officeRepo;
	//Get  Offices by country
		@GetMapping("/offices/country={country}")
		public List<Offices> getOfficeByCountry(@PathVariable("country") String country) {
			return officeRepo.findCountryByLike("%" + country + "%");
		}

		// Get country by city
		@GetMapping("/offices/city={city}")
		public List<Offices> getOfficeByCity(@PathVariable("city") String city) {
			return officeRepo.findOfficeByCityLike(city);
		}
		// id customer DESC
		@GetMapping("/offices/DESC")
		public List<Offices> getOfficeDESCByid() {
			return officeRepo.getOfficeIDdDESC();
		}
//get all office
	@GetMapping("/offices")
	public List<Offices> getAllOffices() {
		return officeRepo.findAll();
	}

// get office by id 
	@GetMapping("/office/{id}")
	public Offices getCustomerById(@PathVariable Long id) {
		if (officeRepo.findById(id).isPresent())
			return officeRepo.findById(id).get();
		else
			return null;
	}
// create  new office
	@PostMapping("/office/create")
	public ResponseEntity<Offices> createNewOffice(@RequestBody Offices cOffice) {
		try {
			Offices newOffices = new Offices();
			newOffices.setCity(cOffice.getCity());
			newOffices.setPhone(cOffice.getPhone());
			newOffices.setAddressLine(cOffice.getAddressLine());
			newOffices.setState(cOffice.getState());
			newOffices.setCountry(cOffice.getCountry());
			newOffices.setTerritory(cOffice.getTerritory());
			Offices saveOffice = officeRepo.save(newOffices);
			return new ResponseEntity<>(saveOffice, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
 // update office by id
	@PutMapping("/office/update/id")
	public ResponseEntity<Object> updateCustomer(@PathVariable("id") Long id, @RequestBody Offices cOffice) {
		Optional<Offices> officesData = officeRepo.findById(id);
		if (officesData.isPresent()) {
			Offices newOffice =  new Offices();
			newOffice.setCity(cOffice.getCity());
			newOffice.setPhone(cOffice.getPhone());
			newOffice.setAddressLine(cOffice.getAddressLine());
			newOffice.setState(cOffice.getState());
			newOffice.setCountry(cOffice.getCountry());
			newOffice.setTerritory(cOffice.getTerritory());
			Offices savedOffice = officeRepo.save(newOffice);
			return new ResponseEntity<>(savedOffice, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
// delete office 
	@DeleteMapping("/office/delete/{id}")
	public ResponseEntity<Object> deleteOfficeById(@PathVariable Long id) {
		try {
			officeRepo.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
