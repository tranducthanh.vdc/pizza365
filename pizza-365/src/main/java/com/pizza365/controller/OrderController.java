package com.pizza365.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pizza365.model.Order;
import com.pizza365.repository.OrderRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class OrderController {
	@Autowired
	private OrderRepository orderRepo;
	//Get  order by 
		@GetMapping("/orders/customerid={customerid}")
		public List<Order> getOrderbyCustomerid(@PathVariable("customerid") String customerId) {
			return orderRepo.findOrderByCustomerIdLike("%" + customerId + "%");
		}
//Get all order
	@GetMapping("/orders")
	public List<Order> getAllOrders() {
		return orderRepo.findAll();
	}

// GET order by id
	@GetMapping("/order/{id}")
	public Order getOrderById(@PathVariable Long id) {
		if (orderRepo.findById(id).isPresent()) {
			return orderRepo.findById(id).get();
		}
		return null;
	}

	// create new customer
	@PostMapping("/order/create")
	public ResponseEntity<Order> createNewOrder(@RequestBody Order cOrder) {
		try {
			Order newOrder = new Order();
			newOrder.setOrderDate(cOrder.getOrderDate());
			newOrder.setStatus(cOrder.getStatus());
			newOrder.setRequiredDate(cOrder.getRequiredDate());
			newOrder.setShippedDate(cOrder.getShippedDate());
			Order saveOrder = orderRepo.save(newOrder);
			return new ResponseEntity<>(saveOrder, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// edit customer by id customer
	@PutMapping("/orders/update/{id}")
	public ResponseEntity<Object> updateOrder(@PathVariable("id") Long id, @RequestBody Order cOrder) {
		Optional<Order> orderData = orderRepo.findById(id);
		if (orderData.isPresent()) {
			Order newOrder = orderData.get();
			newOrder.setOrderDate(cOrder.getOrderDate());
			newOrder.setStatus(cOrder.getStatus());
			newOrder.setRequiredDate(cOrder.getRequiredDate());
			newOrder.setShippedDate(cOrder.getShippedDate());
			Order saveOrder = orderRepo.save(newOrder);
			return new ResponseEntity<>(saveOrder, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	// delete Customer by customer id
	@DeleteMapping("/order/delete/{id}")
	public ResponseEntity<Object> deleteCustomerById(@PathVariable Long id) {
		try {
			orderRepo.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
