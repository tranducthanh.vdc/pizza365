package com.pizza365.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.pizza365.model.Employees;
import com.pizza365.repository.EmployeeRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class EmployeeController {
@Autowired
 private EmployeeRepository employeeRepo;

//Get  employee by lastname
	@GetMapping("/employee/lastname={lastname}")
	public List<Employees> getEmployeeByLastname(@PathVariable("lastname") String lastname) {
		return employeeRepo.findEmployeeByLasnameLike("%" + lastname + "%");
	}

	// Get employee by firstname
	@GetMapping("/employee/firstname={firstname}")
	public List<Employees> getEmployeeByCity(@PathVariable("firstname") String firstname) {
		return employeeRepo.findEmployeeByFirstnameLike(firstname);
	}

	// id employee DESC
	@GetMapping("/employee/DESC")
	public List<Employees> getEmployeeDESCByid() {
		return employeeRepo.getEmployeeIDdDESC();
	}
//Get all employee
	@GetMapping("/employees")
	public List<Employees> getAllEmployees() {
		return employeeRepo.findAll();
	}
	//Get  employee by id
	@GetMapping("/employee/{id}")
	public Employees getOrderdetailById(@PathVariable Long id) {
		if (employeeRepo.findById(id).isPresent())
			return employeeRepo.findById(id).get();
		else
			return null;
	}	
	//  create new employee
	@PostMapping("/employees/create")
	public ResponseEntity<Employees> createNewCustomer(@RequestBody Employees cEmployees) {
		try {
			Employees newEmployee = new Employees();
			newEmployee.setLastName(cEmployees.getLastName());
			newEmployee.setFirstName(cEmployees.getFirstName());
			newEmployee.setExtension(cEmployees.getExtension());
			newEmployee.setEmail(cEmployees.getEmail());
			newEmployee.setOfficeCode(cEmployees.getOfficeCode());
			newEmployee.setReportTo(cEmployees.getReportTo());
			newEmployee.setJobTitle(cEmployees.getJobTitle());
			Employees saveEmployee = employeeRepo.save(newEmployee);
			return new ResponseEntity<>(saveEmployee, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	// edit employee by id employee
			@PutMapping("/employee/update/{id}")
			public ResponseEntity<Object> updateEmployee(@PathVariable("id") Long id, @RequestBody Employees cEmployees) {
				Optional<Employees> employeesData = employeeRepo.findById(id);
				if (employeesData.isPresent()) {
					Employees newEmployee = employeesData.get();
					newEmployee.setLastName(cEmployees.getLastName());
					newEmployee.setFirstName(cEmployees.getFirstName());
					newEmployee.setExtension(cEmployees.getExtension());
					newEmployee.setEmail(cEmployees.getEmail());
					newEmployee.setOfficeCode(cEmployees.getOfficeCode());
					newEmployee.setReportTo(cEmployees.getReportTo());
					newEmployee.setJobTitle(cEmployees.getJobTitle());
					Employees saveEmployee = employeeRepo.save(newEmployee);
					return new ResponseEntity<>(saveEmployee, HttpStatus.OK);
				} else {
					return new ResponseEntity<>(HttpStatus.NOT_FOUND);
				}
			}
			// delete employees by employees id
			@DeleteMapping("/employee/delete/{id}")
			public ResponseEntity<Object> deleteOrderdetailById(@PathVariable Long id) {
				try {
					employeeRepo.deleteById(id);
					return new ResponseEntity<>(HttpStatus.NO_CONTENT);
				} catch (Exception e) {
					System.out.println(e);
					return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
				}
			}	
}
