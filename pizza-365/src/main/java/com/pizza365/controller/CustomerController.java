package com.pizza365.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pizza365.model.*;
import com.pizza365.repository.CustomerRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CustomerController {
	@Autowired
	private CustomerRepository customerRepo;

//Get  customer by lastname
	@GetMapping("/customers/lastname={lastname}")
	public List<Customer> getCustomerByLastname(@PathVariable("lastname") String lastname) {
		return customerRepo.findCustomerByLastnameLike("%" + lastname + "%");
	}

	// Get customer by city
	@GetMapping("/customers/city={city}")
	public List<Customer> getCustomerByCity(@PathVariable("city") String city) {
		return customerRepo.findCustomerByCityLike(city);
	}

	// Get customer by state
	@GetMapping("/customers/state={state}")
	public List<Customer> getCustomerByState(@PathVariable("state") String state) {
		return customerRepo.findCustomerByStateLike(state);
	}

	// id customer DESC
	@GetMapping("/customers/DESC")
	public List<Customer> getCustomerDESCByid() {
		return customerRepo.getCustomerIDdDESC();
	}

	@GetMapping("/customers")
	public List<Customer> getAllCustomer() {
		return customerRepo.findAll();
	}

	// Get customer by id
	@GetMapping("/customer/{id}")
	public Customer getCustomerById(@PathVariable Long id) {
		if (customerRepo.findById(id).isPresent())
			return customerRepo.findById(id).get();
		else
			return null;
	}

	// create new customer
	@PostMapping("/customer/create")
	public ResponseEntity<Customer> createNewCustomer(@RequestBody Customer cCustomer) {
		try {
			Customer newCustomer = new Customer();
			newCustomer.setLastname(cCustomer.getLastname());
			newCustomer.setFirstname(cCustomer.getFirstname());
			newCustomer.setPhoneNumber(cCustomer.getPhoneNumber());
			newCustomer.setAddress(cCustomer.getAddress());
			newCustomer.setCity(cCustomer.getCity());
			newCustomer.setState(cCustomer.getState());
			newCustomer.setPostalCode(cCustomer.getPostalCode());
			newCustomer.setCountry(cCustomer.getCountry());
			newCustomer.setSalesRepEmployeeNumber(cCustomer.getSalesRepEmployeeNumber());
			newCustomer.setCreditLimit(cCustomer.getCreditLimit());
			Customer saveCustomer = customerRepo.save(newCustomer);
			return new ResponseEntity<>(saveCustomer, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	// edit customer by id customer
	@PutMapping("/customer/update/{id}")
	public ResponseEntity<Object> updateCustomer(@PathVariable("id") Long id, @RequestBody Customer cCustomer) {
		Optional<Customer> customerData = customerRepo.findById(id);
		if (customerData.isPresent()) {
			Customer newCustomer = customerData.get();
			newCustomer.setLastname(cCustomer.getLastname());
			newCustomer.setFirstname(cCustomer.getFirstname());
			newCustomer.setPhoneNumber(cCustomer.getPhoneNumber());
			newCustomer.setAddress(cCustomer.getAddress());
			newCustomer.setCity(cCustomer.getCity());
			newCustomer.setState(cCustomer.getState());
			newCustomer.setPostalCode(cCustomer.getPostalCode());
			newCustomer.setCountry(cCustomer.getCountry());
			newCustomer.setSalesRepEmployeeNumber(cCustomer.getSalesRepEmployeeNumber());
			newCustomer.setCreditLimit(cCustomer.getCreditLimit());
			Customer savedCustomer = customerRepo.save(newCustomer);
			return new ResponseEntity<>(savedCustomer, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	// delete Customer by customer id
	@DeleteMapping("/customer/delete/{id}")
	public ResponseEntity<Object> deleteCustomerById(@PathVariable Long id) {
		try {
			customerRepo.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
 //update  when country=null
	@PutMapping("/customer/countryname/{newCountry}")
	public int updateCustomerByCountry(@PathVariable("newCountry" )String country) {	
	 return customerRepo.updateCustomerWithCountry(country);
	}
}
