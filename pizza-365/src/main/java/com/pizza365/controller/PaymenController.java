package com.pizza365.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pizza365.model.Payment;
import com.pizza365.repository.PaymentRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class PaymenController {
@Autowired
 private PaymentRepository paymentRepo;

//Get  payment by customerid
	@GetMapping("/payment/customerId={customerId}")
	public List<Payment> getCustomerByLastname(@PathVariable("customerId") String customerId) {
		return paymentRepo.findPaymentByCustomerIdLike("%" + customerId + "%");
	}

	// Get customer by city
	@GetMapping("/payment/ammount={ammount}")
	public List<Payment> getCustomerByCity(@PathVariable("ammount") String ammount) {
		return paymentRepo.findPaymentByAmountLike(ammount);
	}


@GetMapping("/payments")
public List<Payment> getAllPayment(){
	return paymentRepo.findAll();
}
// Get payment by id
@GetMapping("/payment/{id}")
public Payment getCustomerById(@PathVariable Long id) {
	if (paymentRepo.findById(id).isPresent())
		return paymentRepo.findById(id).get();
	else
		return null;
}

// create new customer
@PostMapping("/payment/create")
public ResponseEntity<Payment> createNewcPayment(@RequestBody Payment cPayment) {
	try {
		Payment newPayment = new Payment();
		newPayment.setCustomerId(cPayment.getCustomerId());
		newPayment.setCheckNumber(cPayment.getCheckNumber());
		newPayment.setPaymentDate(cPayment.getPaymentDate());
		newPayment.setAmmount(cPayment.getAmmount());
		Payment savePayment = paymentRepo.save(newPayment);
		return new ResponseEntity<>(savePayment, HttpStatus.CREATED);
	} catch (Exception e) {
		System.out.println(e);
		return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	}
}

// edit customer by id customer
@PutMapping("/payment/update/{id}")
public ResponseEntity<Object> updateCustomer(@PathVariable("id") Long id, @RequestBody Payment cPayment) {
	Optional<Payment> paymentData = paymentRepo.findById(id);
	if (paymentData.isPresent()) {
		Payment newPayment = paymentData.get();
		newPayment.setCustomerId(cPayment.getCustomerId());
		newPayment.setCheckNumber(cPayment.getCheckNumber());
		newPayment.setPaymentDate(cPayment.getPaymentDate());
		newPayment.setAmmount(cPayment.getAmmount());
		Payment savedPayment = paymentRepo.save(newPayment);
		return new ResponseEntity<>(savedPayment, HttpStatus.OK);
	} else {
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
}
// delete payment by payment id
@DeleteMapping("/payment/delete/{id}")
public ResponseEntity<Object> deletePaymentById(@PathVariable Long id) {
	try {
		paymentRepo.deleteById(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	} catch (Exception e) {
		System.out.println(e);
		return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
}
