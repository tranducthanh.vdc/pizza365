package com.pizza365.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pizza365.model.OrderDetail;
import com.pizza365.repository.OrderDetailRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class OrderDetailController {
	@Autowired
	private OrderDetailRepository orderDetailRepo;

	// Get orderDetail by product_id
	@GetMapping("/orderdetail/productid={product_id}")
	public List<OrderDetail> getOfficeByCountry(@PathVariable("product_id") String productId) {
		return orderDetailRepo.findOrderDetailByProductId("%" + productId + "%");
	}

	// id customer DESC
	@GetMapping("/quantity/DESC")
	public List<OrderDetail> getOrderDetailDESCByQuantity() {
		return orderDetailRepo.getOrderDetailByQuantityOrderDESC();
	}

//Get all order_detail
	@GetMapping("/orderdetails")
	public List<OrderDetail> getAllOrderDetail() {
		return orderDetailRepo.findAll();
	}

	// Get customer by id
	@GetMapping("/orderdetail/{id}")
	public OrderDetail getOrderdetailById(@PathVariable Long id) {
		if (orderDetailRepo.findById(id).isPresent())
			return orderDetailRepo.findById(id).get();
		else
			return null;
	}

// create new orderdetail
	@PostMapping("/orderdetail/create")
	public ResponseEntity<OrderDetail> createNewCustomer(@RequestBody OrderDetail cOrderDetail) {
		try {
			OrderDetail newOrderDetail = new OrderDetail();
			newOrderDetail.setOrderId(cOrderDetail.getOrderId());
			newOrderDetail.setProduct_id(cOrderDetail.getProduct_id());
			newOrderDetail.setQuantityOrder(cOrderDetail.getQuantityOrder());
			newOrderDetail.setPriceEach(cOrderDetail.getPriceEach());
			OrderDetail saveOrderdetail = orderDetailRepo.save(newOrderDetail);
			return new ResponseEntity<>(saveOrderdetail, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// edit customer by id customer
	@PutMapping("/orderdetail/update/{id}")
	public ResponseEntity<Object> updateOrderdetail(@PathVariable("id") Long id,
			@RequestBody OrderDetail cOrderDetail) {
		Optional<OrderDetail> OrderDetailData = orderDetailRepo.findById(id);
		if (OrderDetailData.isPresent()) {
			OrderDetail newOrderDetail = OrderDetailData.get();
			newOrderDetail.setOrderId(cOrderDetail.getOrderId());
			newOrderDetail.setProduct_id(cOrderDetail.getProduct_id());
			newOrderDetail.setQuantityOrder(cOrderDetail.getQuantityOrder());
			newOrderDetail.setPriceEach(cOrderDetail.getPriceEach());
			OrderDetail saveOrderdetail = orderDetailRepo.save(newOrderDetail);
			return new ResponseEntity<>(saveOrderdetail, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	// delete Customer by customer id
	@DeleteMapping("/orderdetail/delete/{id}")
	public ResponseEntity<Object> deleteOrderdetailById(@PathVariable Long id) {
		try {
			orderDetailRepo.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
