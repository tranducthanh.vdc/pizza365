package com.pizza365.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.pizza365.model.ProductLines;
import com.pizza365.repository.ProductLineRepository;



@RestController
@CrossOrigin
@RequestMapping("/")
public class ProductLineController {
@Autowired
private ProductLineRepository productLineRepo;


//Get  by productLine name
	@GetMapping("/productLineName={productLine}")
	public List<ProductLines> getCustomerByLastname(@PathVariable("productLine") String productLine) {
		return productLineRepo.findProductLineByProductLine("%" + productLine + "%");
	}

@GetMapping("/productLines")
public List<ProductLines> getAllProductLine() {
	return productLineRepo.findAll();
}

// Get customer by id
@GetMapping("/productLine/{id}")
public ProductLines getProductLineById(@PathVariable Long id) {
	if (productLineRepo.findById(id).isPresent())
		return productLineRepo.findById(id).get();
	else
		return null;
}

// create new customer
@PostMapping("/productLine/create")
public ResponseEntity<ProductLines> createNewProductLine(@RequestBody ProductLines cProductLines) {
	try {
		ProductLines newProductLines = new ProductLines();
		newProductLines.setProductLine(cProductLines.getDescription());
		newProductLines.setDescription(cProductLines.getDescription());
		ProductLines saveProductLine = productLineRepo.save(newProductLines);
		return new ResponseEntity<>(saveProductLine, HttpStatus.CREATED);
	} catch (Exception e) {
		System.out.println(e);
		return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	}
}

// edit customer by id customer
@PutMapping("/productLine/update/{id}")
public ResponseEntity<Object> updateProductLine(@PathVariable("id") Long id, @RequestBody ProductLines cProductLines) {
	Optional<ProductLines> productLinesData = productLineRepo.findById(id);
	if (productLinesData.isPresent()) {
		ProductLines newProductLines = productLinesData.get();
		newProductLines.setProductLine(cProductLines.getDescription());
		newProductLines.setDescription(cProductLines.getDescription());
		ProductLines saveProductLine = productLineRepo.save(newProductLines);
		return new ResponseEntity<>(saveProductLine, HttpStatus.OK);
	} else {
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
}
// delete Customer by customer id
@DeleteMapping("/productLine/delete/{id}")
public ResponseEntity<Object> deleteProductLineById(@PathVariable Long id) {
	try {
		productLineRepo.deleteById(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	} catch (Exception e) {
		System.out.println(e);
		return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	}
}

}
