package com.pizza365.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "products")
public class Products {
@Id
@GeneratedValue(strategy = GenerationType.AUTO)
private int id;

@Column(name = "product_code")
private String  productCode;

@Column(name = "product_name")
private String  productName;

@Column(name = "product_description")
private String  productDescription;

@Column(name = "product_line_id")
private ProductLines  productLines ;

@Column(name = "product_scale")
private String  productScale;

@Column(name = "product_vendor")
private String  productVendor;

@Column(name = "quantity_in_stock")
private int  quantityInStock;

@Column(name = "buy_price")
private double  buyPrice;

@ManyToOne
@JsonIgnore
private ProductLines productLine;

@OneToMany(mappedBy = "product")
private List<OrderDetail> orderDetail;

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public String getProductCode() {
	return productCode;
}

public void setProductCode(String productCode) {
	this.productCode = productCode;
}

public String getProductName() {
	return productName;
}

public void setProductName(String productName) {
	this.productName = productName;
}

public String getProductDescription() {
	return productDescription;
}

public void setProductDescription(String productDescription) {
	this.productDescription = productDescription;
}

public ProductLines getProductLines() {
	return productLines;
}

public void setProductLines(ProductLines productLines) {
	this.productLines = productLines;
}

public String getProductScale() {
	return productScale;
}

public void setProductScale(String productScale) {
	this.productScale = productScale;
}

public String getProductVendor() {
	return productVendor;
}

public void setProductVendor(String productVendor) {
	this.productVendor = productVendor;
}

public int getQuantityInStock() {
	return quantityInStock;
}

public void setQuantityInStock(int quantityInStock) {
	this.quantityInStock = quantityInStock;
}

public double getBuyPrice() {
	return buyPrice;
}

public void setBuyPrice(double buyPrice) {
	this.buyPrice = buyPrice;
}

public ProductLines getProductLine() {
	return productLine;
}

public void setProductLine(ProductLines productLine) {
	this.productLine = productLine;
}

public List<OrderDetail> getOrderDetail() {
	return orderDetail;
}

public void setOrderDetail(List<OrderDetail> orderDetail) {
	this.orderDetail = orderDetail;
}

public Products(int id, String productCode, String productName, String productDescription, ProductLines productLines,
		String productScale, String productVendor, int quantityInStock, double buyPrice, ProductLines productLine,
		List<OrderDetail> orderDetail) {
	this.id = id;
	this.productCode = productCode;
	this.productName = productName;
	this.productDescription = productDescription;
	this.productLines = productLines;
	this.productScale = productScale;
	this.productVendor = productVendor;
	this.quantityInStock = quantityInStock;
	this.buyPrice = buyPrice;
	this.productLine = productLine;
	this.orderDetail = orderDetail;
}

public Products() {
	
}

}
