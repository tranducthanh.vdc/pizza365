package com.pizza365.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "payments")
public class Payment {

@Id
@GeneratedValue(strategy = GenerationType.AUTO)
private int id;

@Column(name = "customer_id")
private int customerId;

@Column(name = "check_number")
private String checkNumber;

@Temporal(TemporalType.TIMESTAMP)
@Column(name = "payment_date")
private Date paymentDate;

@Column(name = "ammount")
private BigDecimal ammount;

@ManyToOne
@JsonIgnore
private Customer customer;

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public int getCustomerId() {
	return customerId;
}

public void setCustomerId(int customerId) {
	this.customerId = customerId;
}

public String getCheckNumber() {
	return checkNumber;
}

public void setCheckNumber(String checkNumber) {
	this.checkNumber = checkNumber;
}

public Date getPaymentDate() {
	return paymentDate;
}

public void setPaymentDate(Date paymentDate) {
	this.paymentDate = paymentDate;
}

public BigDecimal getAmmount() {
	return ammount;
}

public void setAmmount(BigDecimal ammount) {
	this.ammount = ammount;
}

public Customer getCustomer() {
	return customer;
}

public void setCustomer(Customer customer) {
	this.customer = customer;
}

public Payment(int id, int customerId, String checkNumber, Date paymentDate, BigDecimal ammount, Customer customer) {

	this.id = id;
	this.customerId = customerId;
	this.checkNumber = checkNumber;
	this.paymentDate = paymentDate;
	this.ammount = ammount;
	this.customer = customer;
}

public Payment() {

}


}
