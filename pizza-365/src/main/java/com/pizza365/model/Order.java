package com.pizza365.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "orders")
public class Order {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "order_date", nullable = true, updatable = false)
	@CreatedDate
	@CreationTimestamp
	@JsonFormat(pattern = "dd-MM-yyyy")
	private Date orderDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "required_date", nullable = true, updatable = false)
	@CreatedDate
	@CreationTimestamp
	@JsonFormat(pattern = "dd-MM-yyyy")
	private Date requiredDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "shipped_date", nullable = true, updatable = false)
	@CreatedDate
	@CreationTimestamp
	@JsonFormat(pattern = "dd-MM-yyyy")
	private Date shippedDate;

	@Column(name = "status")
	private String status;

	@Column(name = "comments")
	private String comment;

	@ManyToOne
	private Customer customer;

	@OneToMany(mappedBy = "order")
	private List<OrderDetail> orderDetail;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public Date getRequiredDate() {
		return requiredDate;
	}

	public void setRequiredDate(Date requiredDate) {
		this.requiredDate = requiredDate;
	}

	public Date getShippedDate() {
		return shippedDate;
	}

	public void setShippedDate(Date shippedDate) {
		this.shippedDate = shippedDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public List<OrderDetail> getOrderDetail() {
		return orderDetail;
	}

	public void setOrderDetail(List<OrderDetail> orderDetail) {
		this.orderDetail = orderDetail;
	}

	public Order(int id, Date orderDate, Date requiredDate, Date shippedDate, String status, String comment,
			Customer customer, List<OrderDetail> orderDetail) {
		super();
		this.id = id;
		this.orderDate = orderDate;
		this.requiredDate = requiredDate;
		this.shippedDate = shippedDate;
		this.status = status;
		this.comment = comment;
		this.customer = customer;
		this.orderDetail = orderDetail;
	}

	public Order() {
		super();
	}


}
