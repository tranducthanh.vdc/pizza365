package com.pizza365.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "order_details")
public class OrderDetail {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
    
	@Column(name = "order_id")
	private String orderId;

	@Column(name = "product_id")
	private  String product_id;
	
	@Column(name = "quantity_order")
	private int quantityOrder;
	
	@Column(name = "price_each")
	private double priceEach;
   
	 @ManyToOne
	 @JsonIgnore
	private Products product;
	
	 @ManyToOne
	 @JsonIgnore
	 private Order order;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getProduct_id() {
		return product_id;
	}

	public void setProduct_id(String product_id) {
		this.product_id = product_id;
	}

	public int getQuantityOrder() {
		return quantityOrder;
	}

	public void setQuantityOrder(int quantityOrder) {
		this.quantityOrder = quantityOrder;
	}

	public double getPriceEach() {
		return priceEach;
	}

	public void setPriceEach(double priceEach) {
		this.priceEach = priceEach;
	}

	public Products getProduct() {
		return product;
	}

	public void setProduct(Products product) {
		this.product = product;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public OrderDetail(int id, String orderId, String product_id, int quantityOrder, double priceEach, Products product,
			Order order) {
		this.id = id;
		this.orderId = orderId;
		this.product_id = product_id;
		this.quantityOrder = quantityOrder;
		this.priceEach = priceEach;
		this.product = product;
		this.order = order;
	}

	public OrderDetail() {
	}
	 
	 
	
}
