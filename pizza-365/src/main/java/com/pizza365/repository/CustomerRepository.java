package com.pizza365.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pizza365.model.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long>{	
	
	@Query(value = "FROM  #{#entityName} WHERE last_name like ?1")// LIKE "%ABC%" --> XYABCgh, ABCxyz, gftABC
	List<Customer> findCustomerByLastnameLike(@Param("lastname") String lastname);
	
	@Query(value = "FROM  #{#entityName} WHERE city like ?1")// LIKE "%ABC%" --> XYABCgh, ABCxyz, gftABC
	List<Customer> findCustomerByCityLike(@Param("city") String city);
	
	@Query(value = "FROM  #{#entityName} WHERE state like ?1")// LIKE "%ABC%" --> XYABCgh, ABCxyz, gftABC
	List<Customer> findCustomerByStateLike(@Param("state") String state);
	
	@Query(value = "SELECT * FROM customers ORDER BY id DESC", nativeQuery = true)
	List<Customer> getCustomerIDdDESC();
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE customers SET country = :countryNhap WHERE country IS NULL", nativeQuery = true)
	int updateCustomerWithCountry( @Param("countryNhap") String countryNhap);
}
