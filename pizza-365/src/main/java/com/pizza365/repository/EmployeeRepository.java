package com.pizza365.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import com.pizza365.model.Employees;


public interface EmployeeRepository extends JpaRepository<Employees, Long>{
	@Query(value = "FROM  #{#entityName} WHERE last_name like ?1")// LIKE "%ABC%" --> XYABCgh, ABCxyz, gftABC
	List<Employees> findEmployeeByLasnameLike(@Param("lastname") String lastname);
	
	@Query(value = "FROM  #{#entityName} WHERE first_name like ?1")// LIKE "%ABC%" --> XYABCgh, ABCxyz, gftABC
	List<Employees> findEmployeeByFirstnameLike(@Param("firstname") String firstname);

	@Query(value = "SELECT * FROM offices ORDER BY id DESC", nativeQuery = true)
	List<Employees> getEmployeeIDdDESC();
}
