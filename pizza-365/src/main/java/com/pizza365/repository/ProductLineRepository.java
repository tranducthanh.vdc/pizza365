package com.pizza365.repository;

import java.util.List;



import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import com.pizza365.model.ProductLines;

public interface ProductLineRepository extends JpaRepository<ProductLines, Long>{
	@Query(value = "FROM  #{#entityName} WHERE product_line like ?1")// LIKE "%ABC%" --> XYABCgh, ABCxyz, gftABC
	List<ProductLines> findProductLineByProductLine(@Param("product_line") String productLine);
}
