package com.pizza365.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import com.pizza365.model.Offices;

public interface OfficeRepository extends JpaRepository<Offices, Long> {
	@Query(value = "FROM  #{#entityName} WHERE country like ?1")// LIKE "%ABC%" --> XYABCgh, ABCxyz, gftABC
	List<Offices> findCountryByLike(@Param("country") String country);
	
	@Query(value = "FROM  #{#entityName} WHERE city like ?1")// LIKE "%ABC%" --> XYABCgh, ABCxyz, gftABC
	List<Offices> findOfficeByCityLike(@Param("city") String city);
	
	@Query(value = "SELECT * FROM customers ORDER BY id DESC", nativeQuery = true)
	List<Offices> getOfficeIDdDESC();
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE offices SET country = :countryNhap WHERE country IS NULL", nativeQuery = true)
	int updateOfficeWithCountry( @Param("countryNhap") String countryNhap);
}
