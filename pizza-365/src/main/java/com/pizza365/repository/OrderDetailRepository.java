package com.pizza365.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import com.pizza365.model.OrderDetail;

public interface OrderDetailRepository extends JpaRepository<OrderDetail, Long> {
	
	@Query(value = " FROM  #{#entityName} WHERE product_id like ?1")// LIKE "%ABC%" --> XYABCgh, ABCxyz, gftABC
	List<OrderDetail> findOrderDetailByProductId(@Param("product_id") String productId);
	
	@Query(value = "SELECT * FROM order_details ORDER BY quantity_order DESC", nativeQuery = true)
	List<OrderDetail> getOrderDetailByQuantityOrderDESC();
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE order_details SET country = :countryNhap WHERE country IS NULL", nativeQuery = true)
	int updateOrderDetailWithCountry( @Param("countryNhap") String countryNhap);
}
