package com.pizza365.repository;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import com.pizza365.model.Order;

public interface OrderRepository extends JpaRepository<Order, Long>{
	@Query(value = "FROM  #{#entityName} WHERE customer_id like ?1")// LIKE "%ABC%" --> XYABCgh, ABCxyz, gftABC
	List<Order> findOrderByCustomerIdLike(@Param("customer_id") String customerId);

	@Transactional
	@Modifying
	@Query(value = "UPDATE customers SET country = :countryNhap WHERE country IS NULL", nativeQuery = true)
	int updateCustomerWithCountry( @Param("countryNhap") String countryNhap);
}
