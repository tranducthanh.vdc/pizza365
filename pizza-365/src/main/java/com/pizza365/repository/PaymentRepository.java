package com.pizza365.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.pizza365.model.Payment;

public interface PaymentRepository extends JpaRepository<Payment, Long> {
	@Query(value = "FROM  #{#entityName} WHERE customer_id like ?1")// LIKE "%ABC%" --> XYABCgh, ABCxyz, gftABC
	List<Payment> findPaymentByCustomerIdLike(@Param("customer_id") String customerId);
	
	@Query(value = "FROM  #{#entityName} WHERE ammount like ?1")// LIKE "%ABC%" --> XYABCgh, ABCxyz, gftABC
	List<Payment> findPaymentByAmountLike(@Param("ammount") String ammount);
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE customers SET country = :countryNhap WHERE country IS NULL", nativeQuery = true)
	int updateCustomerWithCountry( @Param("countryNhap") String countryNhap);
}
