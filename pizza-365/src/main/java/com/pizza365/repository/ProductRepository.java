package com.pizza365.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.pizza365.model.Products;

public interface ProductRepository extends JpaRepository<Products, Long>{
	@Query(value = "FROM  #{#entityName} WHERE product_code like ?1")// LIKE "%ABC%" --> XYABCgh, ABCxyz, gftABC
	List<Products> findProductsByCode(@Param("productCode") String productCode);
	
	@Query(value = "FROM  #{#entityName} WHERE quantity_in_stock like ?1")// LIKE "%ABC%" --> XYABCgh, ABCxyz, gftABC
	List<Products> findProductsByQuantity(@Param("quantityStock") String quantityStock);
	
	@Query(value = "SELECT * FROM products ORDER BY buy_price DESC", nativeQuery = true)
	List<Products> getProductsPriceDESC();
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE customers SET country = :countryNhap WHERE country IS NULL", nativeQuery = true)
	int updateCustomerWithCountry( @Param("countryNhap") String countryNhap);
}
